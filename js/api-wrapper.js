const request = require('request');

const api = function(serverDomain) {
	function createResponseHandler(playCB) {
		var lastRoundNumber = 1;
		
		function handleResponse(error, response, body) {
			if (!error && response.statusCode == 200) {
				if (typeof(body) === "string") {
					body = JSON.parse(body);
				}
				
				if (body.error) {
					console.error(body.error);
				}
				else {
					if (body.state.roundNumber != lastRoundNumber) {
						var lastRound = body.state.lastRound;
						console.log("-- End of round. Pot size:", lastRound.pot);
						if (lastRound.yourCard) {
							console.log("-- BOT", lastRound.yourCard, '|', lastRound.opponentCard, 'OPPONENT');
							console.log("-- Winner:", lastRound.winner);
						}
						else {
							if (lastRound.winner == 'BOT') {
								console.log("-- OPPONENT folded");
							}
							else {
								console.log("-- BOT folded");
							}
							console.log("-- Winner:", lastRound.winner);
						}
						
						lastRoundNumber = body.state.roundNumber;
					}
					
					if (body.state.gameOver) {
						playCB(body.state);
						console.log("Game over. Thanks for playing");
					}
					else {
						play(body.gameId, body.state);
					}
				}
			}
			else {
				if (error) {
					console.log(error);
				}
				else {
					console.log(body);
				}
			}
		}
		
		function play(gameId, curState) {
			Promise.resolve(playCB(curState)).then((move) => {
				if (move != null) {
					var options = {
						uri: serverDomain + '/api/play',
						method: 'POST',
						json: {gameId: gameId, action:move}
					};
					
					request.post(options, handleResponse);
				}
			});
		}
		
		return handleResponse;
	}
	
	return {
		newGame: (teamName, opponentId, playCB) => {
			var options = {
				uri: serverDomain + '/api/newgame',
				method: 'POST',
				json: {opponentId: opponentId}
			};
			request.post(options, createResponseHandler(playCB));
		},
		evaluate: (teamName, opponentId, playCB) => {
			var options = {
				uri: serverDomain + '/api/evaluate',
				method: 'POST',
				json: {teamName: teamName}
			};
			request.post(options, createResponseHandler(playCB));
		}
	}
};

module.exports = api;