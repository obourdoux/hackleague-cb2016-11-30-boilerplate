const serverDomain = 'https://01f8729e.ngrok.io';
const apiWrapper = require('./api-wrapper')(serverDomain);

//To run an evaluation instead, replace `newGame` by `evaluate` in the line below
//The number is the opponent ID (only used by newGame).
apiWrapper.newGame(TEAM_NAME, 1, function play(curstate) {
	
	//Simply return the next action you want to do
	if (curstate.gameOver) {
		console.log(curstate);
		return null;
	}
	else if (curstate.turn == 0) {
		return {
			type: "CHECK"
		};
	}
	else if (curstate.priceToFollow < 100) {
		return {
			type: "FOLLOW"
		};
	}
	else {
		return {
			type: "FOLD"
		};
	}
});
