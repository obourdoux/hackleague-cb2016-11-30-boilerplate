﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Json;

namespace HackLeagueCb20161130BoilerPlate
{
    class Program
    {
        static void Main(string[] args)
        {
            new ApiWrapper(SimpleMethod);
            Console.ReadLine();
        }

        public static JsonObject SimpleMethod(JsonObject state)
        {
            dynamic move = new JsonObject();

            move.type = "BET";
            move.amount = 100;

            return move;
        }
    }
}
