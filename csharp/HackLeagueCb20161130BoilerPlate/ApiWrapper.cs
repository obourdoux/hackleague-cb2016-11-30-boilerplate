﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Json;
using System.Net.Http;

namespace HackLeagueCb20161130BoilerPlate
{
    class ApiWrapper
    {
        private static readonly string SERVER_DOMAIN = "http://01f8729e.ngrok.io";
        private static readonly string SERVER_PATH_NEWGAME = "/api/newgame";
        private static readonly string SERVER_PATH_EVALUATE = "/api/evaluate";
        private static readonly string SERVER_PATH_PLAY = "/api/play";

        private HttpClient m_client = new HttpClient();
        private Func<JsonObject, JsonObject> m_method;

        public ApiWrapper(Func<JsonObject, JsonObject> method)
        {
            this.m_method = method;
            this.m_client.BaseAddress = new Uri(SERVER_DOMAIN);

            this.Start();
        }

        private async void NewGame(int opponentId) {
            dynamic parameters = new JsonObject();

            parameters.opponentId = opponentId;

            this.HandleResponse(await this.m_client.PostAsync(SERVER_PATH_NEWGAME, new StringContent(parameters.ToString(), Encoding.UTF8, "application/json")));
        }

        private async void Evaluate(string teamName) {
            dynamic parameters = new JsonObject();

            parameters.teamName = teamName;

            this.HandleResponse(await this.m_client.PostAsync(SERVER_PATH_EVALUATE, new StringContent(parameters.ToString(), Encoding.UTF8, "application/json")));
        }

        private async void HandleResponse(HttpResponseMessage httpResponse)
        {
            if (httpResponse.IsSuccessStatusCode)
            {
                JsonObject response = (JsonObject) JsonObject.Parse(await httpResponse.Content.ReadAsStringAsync());

                Console.WriteLine("Receive: ");
                Console.WriteLine(response.ToString());

                if (!response.ContainsKey("error"))
                {
                    if (Boolean.Parse(response["state"]["gameOver"].ToString()))
                    {
                        Console.WriteLine("Game over. Thanks for playing");
                    }
                    else
                    {
                        this.Play(response);
                    }
                }
                else
                {
                    Console.WriteLine("Error: ");
                    Console.WriteLine(response["error"].ToString());
                }
            }
            else
            {
                Console.WriteLine(httpResponse.StatusCode);
            }
        }

        private async void Play(JsonObject response)
        {
            dynamic parameters = new JsonObject();

            parameters.gameId = response["gameId"];
            parameters.action = this.m_method((JsonObject) response["state"]);

            Console.WriteLine("Send:");
            Console.WriteLine(parameters.ToString());

            this.HandleResponse(await this.m_client.PostAsync(SERVER_PATH_PLAY, new StringContent(parameters.ToString(), Encoding.UTF8, "application/json")));
        }
    }
}
